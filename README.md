# Simons Science School Sep 2024, University of Vienna

Author: Aiko Voigt, IMG, University of Vienna, aiko.voigt-at-univie.ac.at

The git repo shows how to work with ERA5 climate reanalysis data. In particular, it demonstrates how to calculate the time evolution of annual-mean temperature in Vienna over the last decades. One important hope is to covince you that climate data is open and can be used and analyzed by anyone with access to the internet and a laptop, tablet or desktop computer - and of course an interest in topic.

The calculation is in Python, which is a state-of-the-art tool for data science. You will need to become familiar with the tool, yet again the git repo might be an ideal first motivation for this.

The setup of the Python environment is explained below. The Jupyter notebook with the calculation is called era5-vienna.ipynb.

In general, Google - or any search enginge or AI tool - is your friend: there is tons of information online. I do hope you enjoy, so have fun!

Script was developed and run on the IMG Jet cluster JupyterHub. Use 4 Cores with 40GB RAM to make sure to have enough memory.

Credits: 
* Much of the code is based on the ERA5 interactive cookbook of Projet Pythia: https://projectpythia.org/ERA5_interactive-cookbook/README.html.
* I am using the Google Cloud version of ERA5: https://github.com/google-research/arco-era5.

## Python environment and kernel

### IMG Jet cluster

Assume we are working on the IMG jet cluster and that micromamba is installed. We are installing the same environment as is used by the ERA5 interactive cookbook of Projet Pythia.

```
git clone https://github.com/ProjectPythia/ERA5_interactive-cookbook.git
cd ERA5_interactive-cookbook/
micromamba create -f environment.yml
```

This will create the ERA5-interactive environment, located in `~/micromamba/envs/ERA5_interactive/`. We then build a python kernel to be used in JupyterHub.

```
~/micromamba/envs/ERA5_interactive/bin/python3 -m ipykernel install --user --name=ERA5_interactive
```

The kernel is located in `~/.local/share/jupyter/kernels/era5_interactive`.

Note that an environment file environment.yml is also included in the repo. This allows use with mybinder.org, see below.

### mybinder.org

The repo can be used at mybinder.org. 

